package user;

import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 * Created by eric on 2017-02-19.
 */
public interface UserRepositoryInterface {

    int userExistCheck(User user);

    int registerNewUser(User user);

    String getUserId(User user);

    String getUserId(String token);

    String getUserId(PreparedStatement prep);

    ArrayList<String> getLoginTimestamps(String userId);

    int insertTimestamp(String userId);

    int updateToken(String token, String userId);

}
