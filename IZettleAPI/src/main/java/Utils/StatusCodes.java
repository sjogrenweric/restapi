package Utils;

import java.util.HashMap;

/**
 * Created by eric on 2017-02-14.
 * Constants for the api
 */

public class StatusCodes {
    public final static int DB_ERROR = 500;
    public final static int AUTH_ERROR = 505;

    public final static int USERNAME_EXISTS = 400;
    public final static int NOT_FOUND = 401;
    public final static int NO_USER_ERROR = 402;
    public final static int EMAIL_EXISTS = 401;
    public final static int ERROR_TRESH = 300;

    public final static int SUCCESSFUL = 200;

    public final static int UNKWNONW_ERROR = 550;


    //TODO enum

    public static enum StatusCodeDescription {
        DB_ERROR(500);
        private int value;

        private StatusCodeDescription(int value){
            this.value = value;
        }


    }
    public final static HashMap<Integer,String> errorHash = new HashMap<Integer, String>() {{
        put(DB_ERROR, "DB_ERROR");
        put(AUTH_ERROR, "AUTH_ERROR");
        put(USERNAME_EXISTS, "USERNAME_EXISTS");
        put(NOT_FOUND, "NOT_FOUND");
        put(EMAIL_EXISTS, "EMAIL_EXISTS");
        put(ERROR_TRESH, "ERROR_TRESH");
        put(SUCCESSFUL, "SUCCESSFUL");
        put(UNKWNONW_ERROR, "UNKWNONW_ERROR");
        put(NO_USER_ERROR, "NO_USER_ERROR");
    }};

    public static String  getErrorCodeDescription(int key){
        return StatusCodes.errorHash.get(key);
    }

}
