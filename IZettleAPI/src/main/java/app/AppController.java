package app;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by eric on 2017-02-15.
 */
@RestController
public class AppController {


    @RequestMapping("/")
    public String home() {
        return "forward:/index.html";
    }
}
