package Utils;

import org.apache.log4j.Logger;

/**
 * Created by eric on 2017-02-15.
 */
public class Logutil {

    private static final Logger log = Logger.getLogger(Logutil.class.getName());

    /**
     *
     * @param info
     */
    public void info(String info) {
        log.info(info);
    }

    /**
     *
     * @param info
     */
    public void error(String info) {
        log.error(info);
    }

    /**
     *
     * @param info
     */
    public void debug(String info) {
        log.debug(info);
    }


}
