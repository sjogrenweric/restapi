


hashCode = function(s){
  return s.split("").reduce(function(a,b){a=((a<<5)-a)+b.charCodeAt(0);return a&a},0);              
}

var usertoken;

new Vue({
  el: '#nav_reg',
   methods: {
    nav_event: function () {
      reg.seen = true,
      login.seen = false,
      timestamps.seen = false
    }
  }
})

new Vue({
  el: '#nav_login',
   methods: {
    nav_event: function () {
       reg.seen = false,
       login.seen = true,
       timestamps.seen = false
    }
  }
})


var timestamps = new Vue({
  el: "#timestamps",
  data: {
    seen: false,
    timestamp: [],
  },
  methods: {
    times_submit: function(){
       var jsObj = { token: usertoken };
         $.ajax({
          url: 'user/timestamps',
          type: 'get',
          data: jsObj,
          dataType: 'json',
          async: true,
          success: function (msg) {
            timestamps.timestamp = msg.listdata;
            console.log(msg);
           
              
          },
          error: function(err) {
            console.log(err);
            timestamps.timestamp = err;
          }

      });
    }
  }
})

var login = new Vue({
  el: '#login_form',
  data:{
    seen: true,
    info: '',
    username: '',
    password: ''
  },
   methods: {
    login_submit: function () {
      timestamps.seen = true;
      var hashpass = hashCode(this.password+this.username);
      var jsObj = { username: this.username, password: hashpass };
        
         $.ajax({
          url: 'user/login',
          type: 'post',
          dataType: 'json',
          data: jsObj,
          async: true,
          success: function (msg) { 
            usertoken = msg.status;
             console.log(msg);
            //login.info = msg.status;

          },
           error: function(err) {
             console.log(err);
             login.info = err.status;
           }
      });

    }
  }  
})

var reg = new Vue({
  el: '#reg_form',
  data:{
    seen: false,
    info: '',
    username: '',
    password: '',
    email: ''
  },
   methods: {
    register_submit: function () {
      var hashpass = hashCode(this.password+this.username);
      var jsObj = { username: this.username, email: this.email, password: hashpass };
        
         $.ajax({
          url: 'user/register',
          type: 'post',
          dataType: 'json',
          data: jsObj,
          async: true,
          success: function (msg) {
            console.log(msg);
              reg.info = msg.status
          },
          error: function(err) {
             console.log(err);
             reg.info = err.status;
           }
      });

    }
  }
})










