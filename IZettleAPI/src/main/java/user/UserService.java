package user;

import Utils.Logutil;
import Utils.Status;
import Utils.StatusCodes;

import java.util.ArrayList;
import java.util.Collections;
import java.util.UUID;

/**
 * Created by eric on 2017-02-14.
 */
public class UserService {

    UserRepositoryInterface userRepository = new UserRepository();
    Logutil logger = new Logutil();

    /**
     * Registers a user to the service, checks for duplicate emails or usernames.
     *
     * @param user User
     * @return Status
     * @see Status, User
     */
    public Status registerUser(User user) {

        int userStatus = userRepository.userExistCheck(user);
        if (userStatus >= StatusCodes.ERROR_TRESH) {
            return new Status(StatusCodes.getErrorCodeDescription(userStatus), userStatus);
        } else {

            int rowsAffected = userRepository.registerNewUser(user);

            if (rowsAffected == 0) {
                logger.error("NO rows affected in INSERT on timestamp " +
                        StatusCodes.DB_ERROR);
            }
        }
        return new Status(StatusCodes.getErrorCodeDescription(StatusCodes.SUCCESSFUL),
                StatusCodes.SUCCESSFUL);


    }

    /**
     * Login function, Checks if the user exists. If the user exists and the password is correct
     * Create a new token and inserts that into the user table. Creates a timestamp bound to
     * the userid of the user,
     *
     * @param user User
     * @return Status
     * @see Status
     */
    public Status login(User user) {

        String login_data;
        String userId;
        int rowsAffected;


        userId = userRepository.getUserId(user);

        if (userId == null) {
            login_data = StatusCodes.getErrorCodeDescription(StatusCodes.NO_USER_ERROR);
        } else {

            login_data = UUID.randomUUID().toString();

            rowsAffected = userRepository.updateToken(login_data, userId);

            if (rowsAffected == 0) {
                logger.error("NO rows affected in update on token for user " +
                        StatusCodes.NOT_FOUND);
            }

            rowsAffected = userRepository.insertTimestamp(userId);

            if (rowsAffected == 0) {
                logger.error("NO rows affected in INSERT on timestamp " +
                        StatusCodes.NOT_FOUND);
            }

        }

        if (login_data.equals(StatusCodes.getErrorCodeDescription(StatusCodes.NO_USER_ERROR))) {
            return new Status(StatusCodes.getErrorCodeDescription(StatusCodes.NO_USER_ERROR),
                    StatusCodes.NO_USER_ERROR);
        } else {
            return new Status(login_data, StatusCodes.SUCCESSFUL);
        }
    }

    /**
     * Extracts the timestamps for a user bound to a login-token
     * Authenticates that the token is active for a user, then uses the userID
     * bound to that token to get the timestamps.
     *
     * @param token String
     * @return Status
     * @see Status
     */
    public Status getTimestamps(String token) {


        ArrayList<String> timestamps;
        int numberOfResults = 5;
        String userId;

        try {   //Auth user
            userId = authenticateToken(token);
        } catch (IllegalAccessError e) {
            return new Status(StatusCodes.getErrorCodeDescription(StatusCodes.AUTH_ERROR),
                    StatusCodes.AUTH_ERROR);
        } //End auth


        timestamps = userRepository.getLoginTimestamps(userId);


        Collections.reverse(timestamps);
        ArrayList newestTimestamps = new ArrayList<String>();

        for (int i = 0; i < (timestamps.size() >= numberOfResults ? numberOfResults : timestamps.size()); i++) {
            newestTimestamps.add(i, timestamps.get(i));
        }
        return new Status(StatusCodes.getErrorCodeDescription(StatusCodes.SUCCESSFUL),
                newestTimestamps,
                StatusCodes.SUCCESSFUL);


    }

    /**
     * Authentication function
     *
     * @param token user token
     * @return userid or null
     *
     */
    private String authenticateToken(String token) throws IllegalAccessError {


        String userId = userRepository.getUserId(token);
        if (userId == null) {
            logger.error("No users found for token: " + token + " " + StatusCodes.NOT_FOUND);
            throw new IllegalAccessError();
        }
        return userId;

    }
}

