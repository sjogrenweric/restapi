package user;

import Utils.Logutil;
import Utils.StatusCodes;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;

/**
 * Created by eric on 2017-02-14.
 * Database controller, all database communication goes here
 */
public class UserRepository implements UserRepositoryInterface {


    Properties properties = null;
    Connection connection = null;
    String db_schema = "";
    Logutil logger = new Logutil();
    String propertiesFileLocation = "src/main/resources/config.properties";

    public UserRepository() {
        init();
    }

    /**
     * Init function sets up the JDBC connection
     *
     * @see Connection
     */
    private void init() {

        properties = new Properties();

        //READ properties
        try {
            properties.load(new FileInputStream(propertiesFileLocation));
        } catch (IOException e) {
            //e.printStackTrace();
            logger.error("Could not find database properties file: " + propertiesFileLocation);
            System.exit(-1);
        }

        //Setting up JDBC
        logger.info("-------- Setting up JDBC ------------");
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            logger.error("Could not find JDBC driver");
            e.printStackTrace();
            return;
        }
        logger.info("PostgreSQL JDBC Driver found.");


        //Setting up connection
        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://" +
                            properties.getProperty("db_url") +
                            ":" +
                            properties.getProperty("db_port") +
                            "/" +
                            properties.getProperty("db_database"),
                    properties.getProperty("db_user"),
                    properties.getProperty("db_pass"));
            connection.setAutoCommit(false);

        } catch (SQLException e) {
            logger.error("Connection Failed :(!");
            e.printStackTrace();
            return;
        }
        if (connection != null) {
            logger.info("JDBC and postgres working.");
        } else {
            logger.error("Failed to make connection!");
        }
        db_schema = properties.getProperty("db_schema");

    }



    /**
     * Used to check whether a user exists already
     *
     * @param user User
     * @return int information from Constants
     * @see StatusCodes for info
     */
    public int userExistCheck(User user) {
        try {

            String query = "SELECT username, email FROM " + db_schema + ".users WHERE username = ? OR email = ?";
            PreparedStatement prep = connection.prepareStatement(query);
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getEmail());
            ResultSet rs = prep.executeQuery();
            connection.commit();

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    String columnValue = rs.getString(i);
                    if (columnValue.equals(user.getUsername())) {
                        return StatusCodes.USERNAME_EXISTS;
                    } else if (columnValue.equals(user.getEmail())) {
                        return StatusCodes.EMAIL_EXISTS;
                    }
                }
            }
            return StatusCodes.SUCCESSFUL;

        } catch (SQLException e) {
            logger.error("" + StatusCodes.DB_ERROR);
            e.printStackTrace();
        }
        return StatusCodes.DB_ERROR;
    }



    /**
     * registering a new user
     *
     * @param user User
     * @return int
     *
     */
    public int registerNewUser(User user) {

        String query = "INSERT INTO " + db_schema + ".users (userid,username,email,password) VALUES(?,?,?,?)";
        PreparedStatement prep;
        int rowsAffected = 0;

        try {
            prep = connection.prepareStatement(query);
            prep.setString(1, user.getUserid());
            prep.setString(2, user.getUsername());
            prep.setString(3, user.getEmail());
            prep.setString(4, user.getPassword());
            rowsAffected = prep.executeUpdate();
            connection.commit();
            return rowsAffected;
        } catch (SQLException e) {
            logger.error("" + StatusCodes.DB_ERROR);
            e.printStackTrace();
        }
        return rowsAffected;
    }



    /**
     * DB call for getting the userid using username and password
     *
     * @param user String
     * @return String or null
     */
    public String getUserId(User user) {

        String query = "SELECT userid FROM " + db_schema + ".users WHERE username = ? AND password = ?";

        try {
            PreparedStatement prep = connection.prepareStatement(query);
            prep.setString(1, user.getUsername());
            prep.setString(2, user.getPassword());
            return getUserId(prep);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * DB call for getting the userid using token
     *
     * @param token String
     * @return String or null
     */
    public String getUserId(String token) {

        String query = "SELECT userid FROM " + db_schema + ".users WHERE token = ?";
        try {
            PreparedStatement prep = connection.prepareStatement(query);
            prep.setString(1, token);
            return getUserId(prep);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;

    }

    /**
     * Retreives the userid and makes sure its unique
     *
     * @param prep PreparedStatement
     * @return String or null
     */
    public String getUserId(PreparedStatement prep) {

        ResultSet rs;

        try {
            rs = prep.executeQuery();
            connection.commit();
            String userId = "";
            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            int count = 0;
            while (rs.next()) {
                count++;
                for (int i = 1; i <= columnsNumber; i++) {
                    userId = rs.getString(i);
                }
            }
            if (count == 1) {
                return userId;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("" + StatusCodes.DB_ERROR);
        }
        return null;
    }

    /**
     * DB call for Getting timestamps for a userId
     *
     * @param userId String
     * @return ArrayList<String>

     */
    public ArrayList<String> getLoginTimestamps(String userId) {

        ResultSet rs ;
        ArrayList<String> timeStamps = new ArrayList<String>();
        String query = "SELECT timestamp FROM " + db_schema + ".timestamps WHERE userid = ?";

        try {
            PreparedStatement prep = connection.prepareStatement(query);
            prep.setString(1, userId);
            rs = prep.executeQuery();
            connection.commit();

            ResultSetMetaData rsmd = rs.getMetaData();
            int columnsNumber = rsmd.getColumnCount();
            while (rs.next()) {
                for (int i = 1; i <= columnsNumber; i++) {
                    String columnValue = rs.getString(i);
                    timeStamps.add(columnValue);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("" + StatusCodes.DB_ERROR);
        }

        return timeStamps;
    }

    /**
     * DB call for inserting a timestamp for a userId
     *
     * @param userId String
     * @return int
     */
    public int insertTimestamp(String userId) {

        String query;
        PreparedStatement prep;
        int rowsAffected = 0;
        query = "INSERT INTO " + db_schema + ".timestamps (userid,timestamp) VALUES(?,?)";

        try {
            prep = connection.prepareStatement(query);
            prep.setString(1, userId);
            prep.setString(2, new Date().toString());
            rowsAffected = prep.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("" + StatusCodes.DB_ERROR);
        }
        return rowsAffected;
    }

    /**
     * DB call for updating the current token used by the user when it logs in.
     *
     * @param token  String
     * @param userId String
     * @return int rowsAffected
     */
    public int updateToken(String token, String userId) {

        String query;
        PreparedStatement prep;
        int rowsAffected = 0;
        query = "UPDATE " + db_schema + ".users SET token = ? WHERE userid = ?";

        try {
            prep = connection.prepareStatement(query);
            prep.setString(1, token);
            prep.setString(2, userId);
            rowsAffected = prep.executeUpdate();
            connection.commit();
        } catch (SQLException e) {
            e.printStackTrace();
            logger.error("" + StatusCodes.DB_ERROR);
        }
        return rowsAffected;
    }


}



















