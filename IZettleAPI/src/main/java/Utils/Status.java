package Utils;

import java.util.ArrayList;


/**
 * Created by eric on 2017-02-14.
 * Return class for jackson
 */

public class Status {

    public String status;
    public ArrayList<String> listdata;
    public int code;

    public Status(String status, int code) {
        this.status = status;
        this.code = code;
    }

    public Status(String status, ArrayList<String> listData, int code) {
        this.status = status;
        this.listdata = listData;
        this.code = code;
    }

    public Status(int code){
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<String> getListData() {
        return listdata;
    }

    public void setListData(ArrayList<String> listData) {
        this.listdata = listData;
    }

    @Override
    public String toString() {
        return code + "";
    }


}


