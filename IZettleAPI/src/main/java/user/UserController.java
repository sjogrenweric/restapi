package user;

import Utils.Logutil;
import Utils.Status;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.*;


/**
 * Created by eric on 2017-02-14.
 */



@RestController
@EnableAutoConfiguration
@RequestMapping(value = "/user")
public class UserController {


    UserService UserService = new UserService();
    Logutil logger = new Logutil();




    @RequestMapping(value = "/register", method = RequestMethod.POST)
    @ResponseBody
    public Status register(@RequestParam(value = "username", required = true) String username,
                           @RequestParam(value = "email", required = true) String email,
                           @RequestParam(value = "password", required = true) String password) {

        logger.info("Received /regisister request for: username: " + username +
                " email: " + email +
                " password: " + password);

        Status status = UserService.registerUser(new User(username, email, password));

        logger.info("Regisister request for: username: " + username +
                " email: " + email +
                " password: " + password +
                "\n Responded with: " + status);


        return status;

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    @ResponseBody
    public Status login(@RequestParam(value = "username", required = true) String username,
                        @RequestParam(value = "password", required = true) String password) {

        logger.info("Received /login request for: username: " + username + " password: " + password);

        Status status = UserService.login(new User(username, password));

        logger.info(" /login responded for: username: " + username + " password: " + password + " with: " + status);

        return status;

    }


    @RequestMapping(value = "/timestamps", method = RequestMethod.GET)
    @ResponseBody
    public Status timestamps(@RequestParam(value = "token", required = true) String token) {

        logger.info("Received /timestamps request for: token: " + token);

        Status status = UserService.getTimestamps(token);

        logger.info("Responded /timestamps request for: token: " + token + " with: " + status);

        return status;

    }
}
