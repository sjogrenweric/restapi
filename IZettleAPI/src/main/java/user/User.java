package user;

import java.util.UUID;

/**
 * Created by eric on 2017-02-14.
 */
public class User {
    private String username = "";
    private String email = "";
    private String password = "";
    private String userid = "";

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
        this.userid = UUID.randomUUID() + "";
    }

    public User(String username, String password) {
        this.username = username;
        this.password = password;

    }

    public String getUserid() {
        return userid;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }
}
