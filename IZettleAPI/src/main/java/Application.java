import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import user.UserController;

@SpringBootApplication
@ComponentScan
public class Application {


    public static void main(String[] args) throws Exception {
        SpringApplication.run(UserController.class, args);
    }

}